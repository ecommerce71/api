/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : ecommerce

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 21/10/2021 12:05:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for akun
-- ----------------------------
DROP TABLE IF EXISTS `akun`;
CREATE TABLE `akun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `sub` varchar(255) NOT NULL,
  `terakhir_masuk` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipe` varchar(25) NOT NULL DEFAULT 'siswa',
  `dibuat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of akun
-- ----------------------------
BEGIN;
INSERT INTO `akun` VALUES (1, 'randiekas@gmail.com', 'Randi Eka Setiawan', 'https://lh3.googleusercontent.com/a-/AOh14Gi4xaqxMKNwVPnZoD16EBOJZXF-6VMZSUn3PF8FNg=s96-c', '115503907446273485015', '2021-10-21 03:50:11', 'admin', '2021-02-04 16:29:01');
INSERT INTO `akun` VALUES (2, 'randiekas@gmail.com', 'Randi Eka Setiawan', 'https://lh3.googleusercontent.com/a-/AOh14Gi4xaqxMKNwVPnZoD16EBOJZXF-6VMZSUn3PF8FNg=s96-c', '115503907446273485015', '2021-10-21 03:49:59', 'biasa', '2021-02-04 16:29:01');
INSERT INTO `akun` VALUES (19, 'scola.academic@gmail.com', 'Scola LMS', 'https://lh3.googleusercontent.com/a-/AOh14GiYhO4Y0w9I_ijTFwYlIYDbzmHxhWfSryb1-YBHzg=s96-c', '115604379270574927675', '2021-08-27 04:05:39', 'biasa', '2021-08-25 08:46:25');
INSERT INTO `akun` VALUES (20, 'siswadummy07@gmail.com', 'Siswa Dummy', 'https://lh3.googleusercontent.com/a/AATXAJyzX9mi-sZB-WW0JcoEJYFRYF3ElynrSBNTB_8P=s96-c', '113396584626586482015', '2021-09-20 15:52:30', 'biasa', '2021-09-17 18:00:17');
INSERT INTO `akun` VALUES (21, 'konten.scola@gmail.com', 'Konten Scola', 'https://lh3.googleusercontent.com/a/AATXAJxfq27TYn6niZozD706afp3T-JA_E07QeNtC-dn=s96-c', '116333439165571129395', '2021-09-20 15:59:39', 'biasa', '2021-09-20 14:49:01');
INSERT INTO `akun` VALUES (22, 'dheaamelia413@gmail.com', 'Dhea Amelia', 'https://lh3.googleusercontent.com/a-/AOh14GgyUetxnw9oQeMLXs3rOD5hiU8thltuW-0fg7lecA=s96-c', '101673403777113591749', '2021-09-22 16:29:35', 'biasa', '2021-09-21 19:10:14');
INSERT INTO `akun` VALUES (23, 'sandytiman69@gmail.com', 'sandy Cahya', 'https://lh3.googleusercontent.com/a/AATXAJwmeFIoWOrKSCpvOVrCcT5Gp-Y4wEYcjldm6h2i=s96-c', '101075735029645736812', '2021-09-22 13:07:16', 'biasa', '2021-09-22 12:50:45');
INSERT INTO `akun` VALUES (24, 'yudi.subekti@smkn11bdg.sch.id', 'YUDI SUBEKTI, S.Kom 11', 'https://lh3.googleusercontent.com/a/AATXAJyDs7NpLAaLcysl_oypLUdIVLWFJE2SmcAGr2l2=s96-c', '114161401379874443532', '2021-10-06 12:38:29', 'biasa', '2021-09-23 08:44:38');
INSERT INTO `akun` VALUES (25, 'yudi.subekti.skom@gmail.com', 'Yudi Subekti', 'https://lh3.googleusercontent.com/a-/AOh14Gg0Tk9l4MkGrdl3E2N5mZ7XffrH-Ra9PGBxZ6vreA=s96-c', '115944557765161642029', '2021-10-10 07:53:56', 'admin', '2021-09-23 10:22:46');
INSERT INTO `akun` VALUES (26, 'yudi.subekti.skom@gmail.com', 'Yudi Subekti', 'https://lh3.googleusercontent.com/a-/AOh14Gg0Tk9l4MkGrdl3E2N5mZ7XffrH-Ra9PGBxZ6vreA=s96-c', '115944557765161642029', '2021-10-10 07:53:49', 'biasa', '2021-09-23 12:49:22');
INSERT INTO `akun` VALUES (27, 'patriciahenderinaparera@gmail.com', 'PATRICIA HENDERINA PARERA', 'https://lh3.googleusercontent.com/a/AATXAJxW1xWeLYrsnK61SosuHWW8KKf1zyqD1XAvbguQ=s96-c', '113245013034784664570', '2021-10-08 10:39:02', 'biasa', '2021-10-06 09:51:15');
COMMIT;

-- ----------------------------
-- Table structure for bank
-- ----------------------------
DROP TABLE IF EXISTS `bank`;
CREATE TABLE `bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `rekening` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bank
-- ----------------------------
BEGIN;
INSERT INTO `bank` VALUES (1, 'BNI', '1202144193');
INSERT INTO `bank` VALUES (2, 'BCA', '1202144193');
COMMIT;

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banner
-- ----------------------------
BEGIN;
INSERT INTO `banner` VALUES (11, '/uploads/banner/1.png', 'google.com');
INSERT INTO `banner` VALUES (12, '/uploads/banner/2.png', 'google.com');
INSERT INTO `banner` VALUES (13, '/uploads/banner/3.png', 'google.com');
COMMIT;

-- ----------------------------
-- Table structure for iklan
-- ----------------------------
DROP TABLE IF EXISTS `iklan`;
CREATE TABLE `iklan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lokasi` varchar(255) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `dibuat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of iklan
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for jasa
-- ----------------------------
DROP TABLE IF EXISTS `jasa`;
CREATE TABLE `jasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jasa
-- ----------------------------
BEGIN;
INSERT INTO `jasa` VALUES (11, '/uploads/banner/1.png', 'google.com');
INSERT INTO `jasa` VALUES (12, '/uploads/banner/2.png', 'google.com');
INSERT INTO `jasa` VALUES (13, '/uploads/banner/3.png', 'google.com');
COMMIT;

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kategori
-- ----------------------------
BEGIN;
INSERT INTO `kategori` VALUES (1, '/uploads/banner/kue.png', 'Makanan');
INSERT INTO `kategori` VALUES (2, '/uploads/banner/minuman.jpg', 'Minuman');
INSERT INTO `kategori` VALUES (6, '/uploads/banner/pakaian.png', 'Pakaian');
INSERT INTO `kategori` VALUES (7, '/uploads/banner/Syal.png', 'Syal');
INSERT INTO `kategori` VALUES (8, '/uploads/banner/masker.jpeg', 'Masker');
INSERT INTO `kategori` VALUES (9, '/uploads/banner/photo6217436684400832206.jpg', 'Pisang');
COMMIT;

-- ----------------------------
-- Table structure for kota
-- ----------------------------
DROP TABLE IF EXISTS `kota`;
CREATE TABLE `kota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_provinsi` int(11) DEFAULT NULL,
  `tipe` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `kode_pos` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kota_provinsi` (`id_provinsi`),
  CONSTRAINT `kota_provinsi` FOREIGN KEY (`id_provinsi`) REFERENCES `provinsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of kota
-- ----------------------------
BEGIN;
INSERT INTO `kota` VALUES (1, 21, 'Kabupaten', 'Aceh Barat', '23681');
INSERT INTO `kota` VALUES (2, 21, 'Kabupaten', 'Aceh Barat Daya', '23764');
INSERT INTO `kota` VALUES (3, 21, 'Kabupaten', 'Aceh Besar', '23951');
INSERT INTO `kota` VALUES (4, 21, 'Kabupaten', 'Aceh Jaya', '23654');
INSERT INTO `kota` VALUES (5, 21, 'Kabupaten', 'Aceh Selatan', '23719');
INSERT INTO `kota` VALUES (6, 21, 'Kabupaten', 'Aceh Singkil', '24785');
INSERT INTO `kota` VALUES (7, 21, 'Kabupaten', 'Aceh Tamiang', '24476');
INSERT INTO `kota` VALUES (8, 21, 'Kabupaten', 'Aceh Tengah', '24511');
INSERT INTO `kota` VALUES (9, 21, 'Kabupaten', 'Aceh Tenggara', '24611');
INSERT INTO `kota` VALUES (10, 21, 'Kabupaten', 'Aceh Timur', '24454');
INSERT INTO `kota` VALUES (11, 21, 'Kabupaten', 'Aceh Utara', '24382');
INSERT INTO `kota` VALUES (12, 32, 'Kabupaten', 'Agam', '26411');
INSERT INTO `kota` VALUES (13, 23, 'Kabupaten', 'Alor', '85811');
INSERT INTO `kota` VALUES (14, 19, 'Kota', 'Ambon', '97222');
INSERT INTO `kota` VALUES (15, 34, 'Kabupaten', 'Asahan', '21214');
INSERT INTO `kota` VALUES (16, 24, 'Kabupaten', 'Asmat', '99777');
INSERT INTO `kota` VALUES (17, 1, 'Kabupaten', 'Badung', '80351');
INSERT INTO `kota` VALUES (18, 13, 'Kabupaten', 'Balangan', '71611');
INSERT INTO `kota` VALUES (19, 15, 'Kota', 'Balikpapan', '76111');
INSERT INTO `kota` VALUES (20, 21, 'Kota', 'Banda Aceh', '23238');
INSERT INTO `kota` VALUES (21, 18, 'Kota', 'Bandar Lampung', '35139');
INSERT INTO `kota` VALUES (22, 9, 'Kabupaten', 'Bandung', '40311');
INSERT INTO `kota` VALUES (23, 9, 'Kota', 'Bandung', '40111');
INSERT INTO `kota` VALUES (24, 9, 'Kabupaten', 'Bandung Barat', '40721');
INSERT INTO `kota` VALUES (25, 29, 'Kabupaten', 'Banggai', '94711');
INSERT INTO `kota` VALUES (26, 29, 'Kabupaten', 'Banggai Kepulauan', '94881');
INSERT INTO `kota` VALUES (27, 2, 'Kabupaten', 'Bangka', '33212');
INSERT INTO `kota` VALUES (28, 2, 'Kabupaten', 'Bangka Barat', '33315');
INSERT INTO `kota` VALUES (29, 2, 'Kabupaten', 'Bangka Selatan', '33719');
INSERT INTO `kota` VALUES (30, 2, 'Kabupaten', 'Bangka Tengah', '33613');
INSERT INTO `kota` VALUES (31, 11, 'Kabupaten', 'Bangkalan', '69118');
INSERT INTO `kota` VALUES (32, 1, 'Kabupaten', 'Bangli', '80619');
INSERT INTO `kota` VALUES (33, 13, 'Kabupaten', 'Banjar', '70619');
INSERT INTO `kota` VALUES (34, 9, 'Kota', 'Banjar', '46311');
INSERT INTO `kota` VALUES (35, 13, 'Kota', 'Banjarbaru', '70712');
INSERT INTO `kota` VALUES (36, 13, 'Kota', 'Banjarmasin', '70117');
INSERT INTO `kota` VALUES (37, 10, 'Kabupaten', 'Banjarnegara', '53419');
INSERT INTO `kota` VALUES (38, 28, 'Kabupaten', 'Bantaeng', '92411');
INSERT INTO `kota` VALUES (39, 5, 'Kabupaten', 'Bantul', '55715');
INSERT INTO `kota` VALUES (40, 33, 'Kabupaten', 'Banyuasin', '30911');
INSERT INTO `kota` VALUES (41, 10, 'Kabupaten', 'Banyumas', '53114');
INSERT INTO `kota` VALUES (42, 11, 'Kabupaten', 'Banyuwangi', '68416');
INSERT INTO `kota` VALUES (43, 13, 'Kabupaten', 'Barito Kuala', '70511');
INSERT INTO `kota` VALUES (44, 14, 'Kabupaten', 'Barito Selatan', '73711');
INSERT INTO `kota` VALUES (45, 14, 'Kabupaten', 'Barito Timur', '73671');
INSERT INTO `kota` VALUES (46, 14, 'Kabupaten', 'Barito Utara', '73881');
INSERT INTO `kota` VALUES (47, 28, 'Kabupaten', 'Barru', '90719');
INSERT INTO `kota` VALUES (48, 17, 'Kota', 'Batam', '29413');
INSERT INTO `kota` VALUES (49, 10, 'Kabupaten', 'Batang', '51211');
INSERT INTO `kota` VALUES (50, 8, 'Kabupaten', 'Batang Hari', '36613');
INSERT INTO `kota` VALUES (51, 11, 'Kota', 'Batu', '65311');
INSERT INTO `kota` VALUES (52, 34, 'Kabupaten', 'Batu Bara', '21655');
INSERT INTO `kota` VALUES (53, 30, 'Kota', 'Bau-Bau', '93719');
INSERT INTO `kota` VALUES (54, 9, 'Kabupaten', 'Bekasi', '17837');
INSERT INTO `kota` VALUES (55, 9, 'Kota', 'Bekasi', '17121');
INSERT INTO `kota` VALUES (56, 2, 'Kabupaten', 'Belitung', '33419');
INSERT INTO `kota` VALUES (57, 2, 'Kabupaten', 'Belitung Timur', '33519');
INSERT INTO `kota` VALUES (58, 23, 'Kabupaten', 'Belu', '85711');
INSERT INTO `kota` VALUES (59, 21, 'Kabupaten', 'Bener Meriah', '24581');
INSERT INTO `kota` VALUES (60, 26, 'Kabupaten', 'Bengkalis', '28719');
INSERT INTO `kota` VALUES (61, 12, 'Kabupaten', 'Bengkayang', '79213');
INSERT INTO `kota` VALUES (62, 4, 'Kota', 'Bengkulu', '38229');
INSERT INTO `kota` VALUES (63, 4, 'Kabupaten', 'Bengkulu Selatan', '38519');
INSERT INTO `kota` VALUES (64, 4, 'Kabupaten', 'Bengkulu Tengah', '38319');
INSERT INTO `kota` VALUES (65, 4, 'Kabupaten', 'Bengkulu Utara', '38619');
INSERT INTO `kota` VALUES (66, 15, 'Kabupaten', 'Berau', '77311');
INSERT INTO `kota` VALUES (67, 24, 'Kabupaten', 'Biak Numfor', '98119');
INSERT INTO `kota` VALUES (68, 22, 'Kabupaten', 'Bima', '84171');
INSERT INTO `kota` VALUES (69, 22, 'Kota', 'Bima', '84139');
INSERT INTO `kota` VALUES (70, 34, 'Kota', 'Binjai', '20712');
INSERT INTO `kota` VALUES (71, 17, 'Kabupaten', 'Bintan', '29135');
INSERT INTO `kota` VALUES (72, 21, 'Kabupaten', 'Bireuen', '24219');
INSERT INTO `kota` VALUES (73, 31, 'Kota', 'Bitung', '95512');
INSERT INTO `kota` VALUES (74, 11, 'Kabupaten', 'Blitar', '66171');
INSERT INTO `kota` VALUES (75, 11, 'Kota', 'Blitar', '66124');
INSERT INTO `kota` VALUES (76, 10, 'Kabupaten', 'Blora', '58219');
INSERT INTO `kota` VALUES (77, 7, 'Kabupaten', 'Boalemo', '96319');
INSERT INTO `kota` VALUES (78, 9, 'Kabupaten', 'Bogor', '16911');
INSERT INTO `kota` VALUES (79, 9, 'Kota', 'Bogor', '16119');
INSERT INTO `kota` VALUES (80, 11, 'Kabupaten', 'Bojonegoro', '62119');
INSERT INTO `kota` VALUES (81, 31, 'Kabupaten', 'Bolaang Mongondow (Bolmong)', '95755');
INSERT INTO `kota` VALUES (82, 31, 'Kabupaten', 'Bolaang Mongondow Selatan', '95774');
INSERT INTO `kota` VALUES (83, 31, 'Kabupaten', 'Bolaang Mongondow Timur', '95783');
INSERT INTO `kota` VALUES (84, 31, 'Kabupaten', 'Bolaang Mongondow Utara', '95765');
INSERT INTO `kota` VALUES (85, 30, 'Kabupaten', 'Bombana', '93771');
INSERT INTO `kota` VALUES (86, 11, 'Kabupaten', 'Bondowoso', '68219');
INSERT INTO `kota` VALUES (87, 28, 'Kabupaten', 'Bone', '92713');
INSERT INTO `kota` VALUES (88, 7, 'Kabupaten', 'Bone Bolango', '96511');
INSERT INTO `kota` VALUES (89, 15, 'Kota', 'Bontang', '75313');
INSERT INTO `kota` VALUES (90, 24, 'Kabupaten', 'Boven Digoel', '99662');
INSERT INTO `kota` VALUES (91, 10, 'Kabupaten', 'Boyolali', '57312');
INSERT INTO `kota` VALUES (92, 10, 'Kabupaten', 'Brebes', '52212');
INSERT INTO `kota` VALUES (93, 32, 'Kota', 'Bukittinggi', '26115');
INSERT INTO `kota` VALUES (94, 1, 'Kabupaten', 'Buleleng', '81111');
INSERT INTO `kota` VALUES (95, 28, 'Kabupaten', 'Bulukumba', '92511');
INSERT INTO `kota` VALUES (96, 16, 'Kabupaten', 'Bulungan (Bulongan)', '77211');
INSERT INTO `kota` VALUES (97, 8, 'Kabupaten', 'Bungo', '37216');
INSERT INTO `kota` VALUES (98, 29, 'Kabupaten', 'Buol', '94564');
INSERT INTO `kota` VALUES (99, 19, 'Kabupaten', 'Buru', '97371');
INSERT INTO `kota` VALUES (100, 19, 'Kabupaten', 'Buru Selatan', '97351');
INSERT INTO `kota` VALUES (101, 30, 'Kabupaten', 'Buton', '93754');
INSERT INTO `kota` VALUES (102, 30, 'Kabupaten', 'Buton Utara', '93745');
INSERT INTO `kota` VALUES (103, 9, 'Kabupaten', 'Ciamis', '46211');
INSERT INTO `kota` VALUES (104, 9, 'Kabupaten', 'Cianjur', '43217');
INSERT INTO `kota` VALUES (105, 10, 'Kabupaten', 'Cilacap', '53211');
INSERT INTO `kota` VALUES (106, 3, 'Kota', 'Cilegon', '42417');
INSERT INTO `kota` VALUES (107, 9, 'Kota', 'Cimahi', '40512');
INSERT INTO `kota` VALUES (108, 9, 'Kabupaten', 'Cirebon', '45611');
INSERT INTO `kota` VALUES (109, 9, 'Kota', 'Cirebon', '45116');
INSERT INTO `kota` VALUES (110, 34, 'Kabupaten', 'Dairi', '22211');
INSERT INTO `kota` VALUES (111, 24, 'Kabupaten', 'Deiyai (Deliyai)', '98784');
INSERT INTO `kota` VALUES (112, 34, 'Kabupaten', 'Deli Serdang', '20511');
INSERT INTO `kota` VALUES (113, 10, 'Kabupaten', 'Demak', '59519');
INSERT INTO `kota` VALUES (114, 1, 'Kota', 'Denpasar', '80227');
INSERT INTO `kota` VALUES (115, 9, 'Kota', 'Depok', '16416');
INSERT INTO `kota` VALUES (116, 32, 'Kabupaten', 'Dharmasraya', '27612');
INSERT INTO `kota` VALUES (117, 24, 'Kabupaten', 'Dogiyai', '98866');
INSERT INTO `kota` VALUES (118, 22, 'Kabupaten', 'Dompu', '84217');
INSERT INTO `kota` VALUES (119, 29, 'Kabupaten', 'Donggala', '94341');
INSERT INTO `kota` VALUES (120, 26, 'Kota', 'Dumai', '28811');
INSERT INTO `kota` VALUES (121, 33, 'Kabupaten', 'Empat Lawang', '31811');
INSERT INTO `kota` VALUES (122, 23, 'Kabupaten', 'Ende', '86351');
INSERT INTO `kota` VALUES (123, 28, 'Kabupaten', 'Enrekang', '91719');
INSERT INTO `kota` VALUES (124, 25, 'Kabupaten', 'Fakfak', '98651');
INSERT INTO `kota` VALUES (125, 23, 'Kabupaten', 'Flores Timur', '86213');
INSERT INTO `kota` VALUES (126, 9, 'Kabupaten', 'Garut', '44126');
INSERT INTO `kota` VALUES (127, 21, 'Kabupaten', 'Gayo Lues', '24653');
INSERT INTO `kota` VALUES (128, 1, 'Kabupaten', 'Gianyar', '80519');
INSERT INTO `kota` VALUES (129, 7, 'Kabupaten', 'Gorontalo', '96218');
INSERT INTO `kota` VALUES (130, 7, 'Kota', 'Gorontalo', '96115');
INSERT INTO `kota` VALUES (131, 7, 'Kabupaten', 'Gorontalo Utara', '96611');
INSERT INTO `kota` VALUES (132, 28, 'Kabupaten', 'Gowa', '92111');
INSERT INTO `kota` VALUES (133, 11, 'Kabupaten', 'Gresik', '61115');
INSERT INTO `kota` VALUES (134, 10, 'Kabupaten', 'Grobogan', '58111');
INSERT INTO `kota` VALUES (135, 5, 'Kabupaten', 'Gunung Kidul', '55812');
INSERT INTO `kota` VALUES (136, 14, 'Kabupaten', 'Gunung Mas', '74511');
INSERT INTO `kota` VALUES (137, 34, 'Kota', 'Gunungsitoli', '22813');
INSERT INTO `kota` VALUES (138, 20, 'Kabupaten', 'Halmahera Barat', '97757');
INSERT INTO `kota` VALUES (139, 20, 'Kabupaten', 'Halmahera Selatan', '97911');
INSERT INTO `kota` VALUES (140, 20, 'Kabupaten', 'Halmahera Tengah', '97853');
INSERT INTO `kota` VALUES (141, 20, 'Kabupaten', 'Halmahera Timur', '97862');
INSERT INTO `kota` VALUES (142, 20, 'Kabupaten', 'Halmahera Utara', '97762');
INSERT INTO `kota` VALUES (143, 13, 'Kabupaten', 'Hulu Sungai Selatan', '71212');
INSERT INTO `kota` VALUES (144, 13, 'Kabupaten', 'Hulu Sungai Tengah', '71313');
INSERT INTO `kota` VALUES (145, 13, 'Kabupaten', 'Hulu Sungai Utara', '71419');
INSERT INTO `kota` VALUES (146, 34, 'Kabupaten', 'Humbang Hasundutan', '22457');
INSERT INTO `kota` VALUES (147, 26, 'Kabupaten', 'Indragiri Hilir', '29212');
INSERT INTO `kota` VALUES (148, 26, 'Kabupaten', 'Indragiri Hulu', '29319');
INSERT INTO `kota` VALUES (149, 9, 'Kabupaten', 'Indramayu', '45214');
INSERT INTO `kota` VALUES (150, 24, 'Kabupaten', 'Intan Jaya', '98771');
INSERT INTO `kota` VALUES (151, 6, 'Kota', 'Jakarta Barat', '11220');
INSERT INTO `kota` VALUES (152, 6, 'Kota', 'Jakarta Pusat', '10540');
INSERT INTO `kota` VALUES (153, 6, 'Kota', 'Jakarta Selatan', '12230');
INSERT INTO `kota` VALUES (154, 6, 'Kota', 'Jakarta Timur', '13330');
INSERT INTO `kota` VALUES (155, 6, 'Kota', 'Jakarta Utara', '14140');
INSERT INTO `kota` VALUES (156, 8, 'Kota', 'Jambi', '36111');
INSERT INTO `kota` VALUES (157, 24, 'Kabupaten', 'Jayapura', '99352');
INSERT INTO `kota` VALUES (158, 24, 'Kota', 'Jayapura', '99114');
INSERT INTO `kota` VALUES (159, 24, 'Kabupaten', 'Jayawijaya', '99511');
INSERT INTO `kota` VALUES (160, 11, 'Kabupaten', 'Jember', '68113');
INSERT INTO `kota` VALUES (161, 1, 'Kabupaten', 'Jembrana', '82251');
INSERT INTO `kota` VALUES (162, 28, 'Kabupaten', 'Jeneponto', '92319');
INSERT INTO `kota` VALUES (163, 10, 'Kabupaten', 'Jepara', '59419');
INSERT INTO `kota` VALUES (164, 11, 'Kabupaten', 'Jombang', '61415');
INSERT INTO `kota` VALUES (165, 25, 'Kabupaten', 'Kaimana', '98671');
INSERT INTO `kota` VALUES (166, 26, 'Kabupaten', 'Kampar', '28411');
INSERT INTO `kota` VALUES (167, 14, 'Kabupaten', 'Kapuas', '73583');
INSERT INTO `kota` VALUES (168, 12, 'Kabupaten', 'Kapuas Hulu', '78719');
INSERT INTO `kota` VALUES (169, 10, 'Kabupaten', 'Karanganyar', '57718');
INSERT INTO `kota` VALUES (170, 1, 'Kabupaten', 'Karangasem', '80819');
INSERT INTO `kota` VALUES (171, 9, 'Kabupaten', 'Karawang', '41311');
INSERT INTO `kota` VALUES (172, 17, 'Kabupaten', 'Karimun', '29611');
INSERT INTO `kota` VALUES (173, 34, 'Kabupaten', 'Karo', '22119');
INSERT INTO `kota` VALUES (174, 14, 'Kabupaten', 'Katingan', '74411');
INSERT INTO `kota` VALUES (175, 4, 'Kabupaten', 'Kaur', '38911');
INSERT INTO `kota` VALUES (176, 12, 'Kabupaten', 'Kayong Utara', '78852');
INSERT INTO `kota` VALUES (177, 10, 'Kabupaten', 'Kebumen', '54319');
INSERT INTO `kota` VALUES (178, 11, 'Kabupaten', 'Kediri', '64184');
INSERT INTO `kota` VALUES (179, 11, 'Kota', 'Kediri', '64125');
INSERT INTO `kota` VALUES (180, 24, 'Kabupaten', 'Keerom', '99461');
INSERT INTO `kota` VALUES (181, 10, 'Kabupaten', 'Kendal', '51314');
INSERT INTO `kota` VALUES (182, 30, 'Kota', 'Kendari', '93126');
INSERT INTO `kota` VALUES (183, 4, 'Kabupaten', 'Kepahiang', '39319');
INSERT INTO `kota` VALUES (184, 17, 'Kabupaten', 'Kepulauan Anambas', '29991');
INSERT INTO `kota` VALUES (185, 19, 'Kabupaten', 'Kepulauan Aru', '97681');
INSERT INTO `kota` VALUES (186, 32, 'Kabupaten', 'Kepulauan Mentawai', '25771');
INSERT INTO `kota` VALUES (187, 26, 'Kabupaten', 'Kepulauan Meranti', '28791');
INSERT INTO `kota` VALUES (188, 31, 'Kabupaten', 'Kepulauan Sangihe', '95819');
INSERT INTO `kota` VALUES (189, 6, 'Kabupaten', 'Kepulauan Seribu', '14550');
INSERT INTO `kota` VALUES (190, 31, 'Kabupaten', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', '95862');
INSERT INTO `kota` VALUES (191, 20, 'Kabupaten', 'Kepulauan Sula', '97995');
INSERT INTO `kota` VALUES (192, 31, 'Kabupaten', 'Kepulauan Talaud', '95885');
INSERT INTO `kota` VALUES (193, 24, 'Kabupaten', 'Kepulauan Yapen (Yapen Waropen)', '98211');
INSERT INTO `kota` VALUES (194, 8, 'Kabupaten', 'Kerinci', '37167');
INSERT INTO `kota` VALUES (195, 12, 'Kabupaten', 'Ketapang', '78874');
INSERT INTO `kota` VALUES (196, 10, 'Kabupaten', 'Klaten', '57411');
INSERT INTO `kota` VALUES (197, 1, 'Kabupaten', 'Klungkung', '80719');
INSERT INTO `kota` VALUES (198, 30, 'Kabupaten', 'Kolaka', '93511');
INSERT INTO `kota` VALUES (199, 30, 'Kabupaten', 'Kolaka Utara', '93911');
INSERT INTO `kota` VALUES (200, 30, 'Kabupaten', 'Konawe', '93411');
INSERT INTO `kota` VALUES (201, 30, 'Kabupaten', 'Konawe Selatan', '93811');
INSERT INTO `kota` VALUES (202, 30, 'Kabupaten', 'Konawe Utara', '93311');
INSERT INTO `kota` VALUES (203, 13, 'Kabupaten', 'Kotabaru', '72119');
INSERT INTO `kota` VALUES (204, 31, 'Kota', 'Kotamobagu', '95711');
INSERT INTO `kota` VALUES (205, 14, 'Kabupaten', 'Kotawaringin Barat', '74119');
INSERT INTO `kota` VALUES (206, 14, 'Kabupaten', 'Kotawaringin Timur', '74364');
INSERT INTO `kota` VALUES (207, 26, 'Kabupaten', 'Kuantan Singingi', '29519');
INSERT INTO `kota` VALUES (208, 12, 'Kabupaten', 'Kubu Raya', '78311');
INSERT INTO `kota` VALUES (209, 10, 'Kabupaten', 'Kudus', '59311');
INSERT INTO `kota` VALUES (210, 5, 'Kabupaten', 'Kulon Progo', '55611');
INSERT INTO `kota` VALUES (211, 9, 'Kabupaten', 'Kuningan', '45511');
INSERT INTO `kota` VALUES (212, 23, 'Kabupaten', 'Kupang', '85362');
INSERT INTO `kota` VALUES (213, 23, 'Kota', 'Kupang', '85119');
INSERT INTO `kota` VALUES (214, 15, 'Kabupaten', 'Kutai Barat', '75711');
INSERT INTO `kota` VALUES (215, 15, 'Kabupaten', 'Kutai Kartanegara', '75511');
INSERT INTO `kota` VALUES (216, 15, 'Kabupaten', 'Kutai Timur', '75611');
INSERT INTO `kota` VALUES (217, 34, 'Kabupaten', 'Labuhan Batu', '21412');
INSERT INTO `kota` VALUES (218, 34, 'Kabupaten', 'Labuhan Batu Selatan', '21511');
INSERT INTO `kota` VALUES (219, 34, 'Kabupaten', 'Labuhan Batu Utara', '21711');
INSERT INTO `kota` VALUES (220, 33, 'Kabupaten', 'Lahat', '31419');
INSERT INTO `kota` VALUES (221, 14, 'Kabupaten', 'Lamandau', '74611');
INSERT INTO `kota` VALUES (222, 11, 'Kabupaten', 'Lamongan', '64125');
INSERT INTO `kota` VALUES (223, 18, 'Kabupaten', 'Lampung Barat', '34814');
INSERT INTO `kota` VALUES (224, 18, 'Kabupaten', 'Lampung Selatan', '35511');
INSERT INTO `kota` VALUES (225, 18, 'Kabupaten', 'Lampung Tengah', '34212');
INSERT INTO `kota` VALUES (226, 18, 'Kabupaten', 'Lampung Timur', '34319');
INSERT INTO `kota` VALUES (227, 18, 'Kabupaten', 'Lampung Utara', '34516');
INSERT INTO `kota` VALUES (228, 12, 'Kabupaten', 'Landak', '78319');
INSERT INTO `kota` VALUES (229, 34, 'Kabupaten', 'Langkat', '20811');
INSERT INTO `kota` VALUES (230, 21, 'Kota', 'Langsa', '24412');
INSERT INTO `kota` VALUES (231, 24, 'Kabupaten', 'Lanny Jaya', '99531');
INSERT INTO `kota` VALUES (232, 3, 'Kabupaten', 'Lebak', '42319');
INSERT INTO `kota` VALUES (233, 4, 'Kabupaten', 'Lebong', '39264');
INSERT INTO `kota` VALUES (234, 23, 'Kabupaten', 'Lembata', '86611');
INSERT INTO `kota` VALUES (235, 21, 'Kota', 'Lhokseumawe', '24352');
INSERT INTO `kota` VALUES (236, 32, 'Kabupaten', 'Lima Puluh Koto/Kota', '26671');
INSERT INTO `kota` VALUES (237, 17, 'Kabupaten', 'Lingga', '29811');
INSERT INTO `kota` VALUES (238, 22, 'Kabupaten', 'Lombok Barat', '83311');
INSERT INTO `kota` VALUES (239, 22, 'Kabupaten', 'Lombok Tengah', '83511');
INSERT INTO `kota` VALUES (240, 22, 'Kabupaten', 'Lombok Timur', '83612');
INSERT INTO `kota` VALUES (241, 22, 'Kabupaten', 'Lombok Utara', '83711');
INSERT INTO `kota` VALUES (242, 33, 'Kota', 'Lubuk Linggau', '31614');
INSERT INTO `kota` VALUES (243, 11, 'Kabupaten', 'Lumajang', '67319');
INSERT INTO `kota` VALUES (244, 28, 'Kabupaten', 'Luwu', '91994');
INSERT INTO `kota` VALUES (245, 28, 'Kabupaten', 'Luwu Timur', '92981');
INSERT INTO `kota` VALUES (246, 28, 'Kabupaten', 'Luwu Utara', '92911');
INSERT INTO `kota` VALUES (247, 11, 'Kabupaten', 'Madiun', '63153');
INSERT INTO `kota` VALUES (248, 11, 'Kota', 'Madiun', '63122');
INSERT INTO `kota` VALUES (249, 10, 'Kabupaten', 'Magelang', '56519');
INSERT INTO `kota` VALUES (250, 10, 'Kota', 'Magelang', '56133');
INSERT INTO `kota` VALUES (251, 11, 'Kabupaten', 'Magetan', '63314');
INSERT INTO `kota` VALUES (252, 9, 'Kabupaten', 'Majalengka', '45412');
INSERT INTO `kota` VALUES (253, 27, 'Kabupaten', 'Majene', '91411');
INSERT INTO `kota` VALUES (254, 28, 'Kota', 'Makassar', '90111');
INSERT INTO `kota` VALUES (255, 11, 'Kabupaten', 'Malang', '65163');
INSERT INTO `kota` VALUES (256, 11, 'Kota', 'Malang', '65112');
INSERT INTO `kota` VALUES (257, 16, 'Kabupaten', 'Malinau', '77511');
INSERT INTO `kota` VALUES (258, 19, 'Kabupaten', 'Maluku Barat Daya', '97451');
INSERT INTO `kota` VALUES (259, 19, 'Kabupaten', 'Maluku Tengah', '97513');
INSERT INTO `kota` VALUES (260, 19, 'Kabupaten', 'Maluku Tenggara', '97651');
INSERT INTO `kota` VALUES (261, 19, 'Kabupaten', 'Maluku Tenggara Barat', '97465');
INSERT INTO `kota` VALUES (262, 27, 'Kabupaten', 'Mamasa', '91362');
INSERT INTO `kota` VALUES (263, 24, 'Kabupaten', 'Mamberamo Raya', '99381');
INSERT INTO `kota` VALUES (264, 24, 'Kabupaten', 'Mamberamo Tengah', '99553');
INSERT INTO `kota` VALUES (265, 27, 'Kabupaten', 'Mamuju', '91519');
INSERT INTO `kota` VALUES (266, 27, 'Kabupaten', 'Mamuju Utara', '91571');
INSERT INTO `kota` VALUES (267, 31, 'Kota', 'Manado', '95247');
INSERT INTO `kota` VALUES (268, 34, 'Kabupaten', 'Mandailing Natal', '22916');
INSERT INTO `kota` VALUES (269, 23, 'Kabupaten', 'Manggarai', '86551');
INSERT INTO `kota` VALUES (270, 23, 'Kabupaten', 'Manggarai Barat', '86711');
INSERT INTO `kota` VALUES (271, 23, 'Kabupaten', 'Manggarai Timur', '86811');
INSERT INTO `kota` VALUES (272, 25, 'Kabupaten', 'Manokwari', '98311');
INSERT INTO `kota` VALUES (273, 25, 'Kabupaten', 'Manokwari Selatan', '98355');
INSERT INTO `kota` VALUES (274, 24, 'Kabupaten', 'Mappi', '99853');
INSERT INTO `kota` VALUES (275, 28, 'Kabupaten', 'Maros', '90511');
INSERT INTO `kota` VALUES (276, 22, 'Kota', 'Mataram', '83131');
INSERT INTO `kota` VALUES (277, 25, 'Kabupaten', 'Maybrat', '98051');
INSERT INTO `kota` VALUES (278, 34, 'Kota', 'Medan', '20228');
INSERT INTO `kota` VALUES (279, 12, 'Kabupaten', 'Melawi', '78619');
INSERT INTO `kota` VALUES (280, 8, 'Kabupaten', 'Merangin', '37319');
INSERT INTO `kota` VALUES (281, 24, 'Kabupaten', 'Merauke', '99613');
INSERT INTO `kota` VALUES (282, 18, 'Kabupaten', 'Mesuji', '34911');
INSERT INTO `kota` VALUES (283, 18, 'Kota', 'Metro', '34111');
INSERT INTO `kota` VALUES (284, 24, 'Kabupaten', 'Mimika', '99962');
INSERT INTO `kota` VALUES (285, 31, 'Kabupaten', 'Minahasa', '95614');
INSERT INTO `kota` VALUES (286, 31, 'Kabupaten', 'Minahasa Selatan', '95914');
INSERT INTO `kota` VALUES (287, 31, 'Kabupaten', 'Minahasa Tenggara', '95995');
INSERT INTO `kota` VALUES (288, 31, 'Kabupaten', 'Minahasa Utara', '95316');
INSERT INTO `kota` VALUES (289, 11, 'Kabupaten', 'Mojokerto', '61382');
INSERT INTO `kota` VALUES (290, 11, 'Kota', 'Mojokerto', '61316');
INSERT INTO `kota` VALUES (291, 29, 'Kabupaten', 'Morowali', '94911');
INSERT INTO `kota` VALUES (292, 33, 'Kabupaten', 'Muara Enim', '31315');
INSERT INTO `kota` VALUES (293, 8, 'Kabupaten', 'Muaro Jambi', '36311');
INSERT INTO `kota` VALUES (294, 4, 'Kabupaten', 'Muko Muko', '38715');
INSERT INTO `kota` VALUES (295, 30, 'Kabupaten', 'Muna', '93611');
INSERT INTO `kota` VALUES (296, 14, 'Kabupaten', 'Murung Raya', '73911');
INSERT INTO `kota` VALUES (297, 33, 'Kabupaten', 'Musi Banyuasin', '30719');
INSERT INTO `kota` VALUES (298, 33, 'Kabupaten', 'Musi Rawas', '31661');
INSERT INTO `kota` VALUES (299, 24, 'Kabupaten', 'Nabire', '98816');
INSERT INTO `kota` VALUES (300, 21, 'Kabupaten', 'Nagan Raya', '23674');
INSERT INTO `kota` VALUES (301, 23, 'Kabupaten', 'Nagekeo', '86911');
INSERT INTO `kota` VALUES (302, 17, 'Kabupaten', 'Natuna', '29711');
INSERT INTO `kota` VALUES (303, 24, 'Kabupaten', 'Nduga', '99541');
INSERT INTO `kota` VALUES (304, 23, 'Kabupaten', 'Ngada', '86413');
INSERT INTO `kota` VALUES (305, 11, 'Kabupaten', 'Nganjuk', '64414');
INSERT INTO `kota` VALUES (306, 11, 'Kabupaten', 'Ngawi', '63219');
INSERT INTO `kota` VALUES (307, 34, 'Kabupaten', 'Nias', '22876');
INSERT INTO `kota` VALUES (308, 34, 'Kabupaten', 'Nias Barat', '22895');
INSERT INTO `kota` VALUES (309, 34, 'Kabupaten', 'Nias Selatan', '22865');
INSERT INTO `kota` VALUES (310, 34, 'Kabupaten', 'Nias Utara', '22856');
INSERT INTO `kota` VALUES (311, 16, 'Kabupaten', 'Nunukan', '77421');
INSERT INTO `kota` VALUES (312, 33, 'Kabupaten', 'Ogan Ilir', '30811');
INSERT INTO `kota` VALUES (313, 33, 'Kabupaten', 'Ogan Komering Ilir', '30618');
INSERT INTO `kota` VALUES (314, 33, 'Kabupaten', 'Ogan Komering Ulu', '32112');
INSERT INTO `kota` VALUES (315, 33, 'Kabupaten', 'Ogan Komering Ulu Selatan', '32211');
INSERT INTO `kota` VALUES (316, 33, 'Kabupaten', 'Ogan Komering Ulu Timur', '32312');
INSERT INTO `kota` VALUES (317, 11, 'Kabupaten', 'Pacitan', '63512');
INSERT INTO `kota` VALUES (318, 32, 'Kota', 'Padang', '25112');
INSERT INTO `kota` VALUES (319, 34, 'Kabupaten', 'Padang Lawas', '22763');
INSERT INTO `kota` VALUES (320, 34, 'Kabupaten', 'Padang Lawas Utara', '22753');
INSERT INTO `kota` VALUES (321, 32, 'Kota', 'Padang Panjang', '27122');
INSERT INTO `kota` VALUES (322, 32, 'Kabupaten', 'Padang Pariaman', '25583');
INSERT INTO `kota` VALUES (323, 34, 'Kota', 'Padang Sidempuan', '22727');
INSERT INTO `kota` VALUES (324, 33, 'Kota', 'Pagar Alam', '31512');
INSERT INTO `kota` VALUES (325, 34, 'Kabupaten', 'Pakpak Bharat', '22272');
INSERT INTO `kota` VALUES (326, 14, 'Kota', 'Palangka Raya', '73112');
INSERT INTO `kota` VALUES (327, 33, 'Kota', 'Palembang', '30111');
INSERT INTO `kota` VALUES (328, 28, 'Kota', 'Palopo', '91911');
INSERT INTO `kota` VALUES (329, 29, 'Kota', 'Palu', '94111');
INSERT INTO `kota` VALUES (330, 11, 'Kabupaten', 'Pamekasan', '69319');
INSERT INTO `kota` VALUES (331, 3, 'Kabupaten', 'Pandeglang', '42212');
INSERT INTO `kota` VALUES (332, 9, 'Kabupaten', 'Pangandaran', '46511');
INSERT INTO `kota` VALUES (333, 28, 'Kabupaten', 'Pangkajene Kepulauan', '90611');
INSERT INTO `kota` VALUES (334, 2, 'Kota', 'Pangkal Pinang', '33115');
INSERT INTO `kota` VALUES (335, 24, 'Kabupaten', 'Paniai', '98765');
INSERT INTO `kota` VALUES (336, 28, 'Kota', 'Parepare', '91123');
INSERT INTO `kota` VALUES (337, 32, 'Kota', 'Pariaman', '25511');
INSERT INTO `kota` VALUES (338, 29, 'Kabupaten', 'Parigi Moutong', '94411');
INSERT INTO `kota` VALUES (339, 32, 'Kabupaten', 'Pasaman', '26318');
INSERT INTO `kota` VALUES (340, 32, 'Kabupaten', 'Pasaman Barat', '26511');
INSERT INTO `kota` VALUES (341, 15, 'Kabupaten', 'Paser', '76211');
INSERT INTO `kota` VALUES (342, 11, 'Kabupaten', 'Pasuruan', '67153');
INSERT INTO `kota` VALUES (343, 11, 'Kota', 'Pasuruan', '67118');
INSERT INTO `kota` VALUES (344, 10, 'Kabupaten', 'Pati', '59114');
INSERT INTO `kota` VALUES (345, 32, 'Kota', 'Payakumbuh', '26213');
INSERT INTO `kota` VALUES (346, 25, 'Kabupaten', 'Pegunungan Arfak', '98354');
INSERT INTO `kota` VALUES (347, 24, 'Kabupaten', 'Pegunungan Bintang', '99573');
INSERT INTO `kota` VALUES (348, 10, 'Kabupaten', 'Pekalongan', '51161');
INSERT INTO `kota` VALUES (349, 10, 'Kota', 'Pekalongan', '51122');
INSERT INTO `kota` VALUES (350, 26, 'Kota', 'Pekanbaru', '28112');
INSERT INTO `kota` VALUES (351, 26, 'Kabupaten', 'Pelalawan', '28311');
INSERT INTO `kota` VALUES (352, 10, 'Kabupaten', 'Pemalang', '52319');
INSERT INTO `kota` VALUES (353, 34, 'Kota', 'Pematang Siantar', '21126');
INSERT INTO `kota` VALUES (354, 15, 'Kabupaten', 'Penajam Paser Utara', '76311');
INSERT INTO `kota` VALUES (355, 18, 'Kabupaten', 'Pesawaran', '35312');
INSERT INTO `kota` VALUES (356, 18, 'Kabupaten', 'Pesisir Barat', '35974');
INSERT INTO `kota` VALUES (357, 32, 'Kabupaten', 'Pesisir Selatan', '25611');
INSERT INTO `kota` VALUES (358, 21, 'Kabupaten', 'Pidie', '24116');
INSERT INTO `kota` VALUES (359, 21, 'Kabupaten', 'Pidie Jaya', '24186');
INSERT INTO `kota` VALUES (360, 28, 'Kabupaten', 'Pinrang', '91251');
INSERT INTO `kota` VALUES (361, 7, 'Kabupaten', 'Pohuwato', '96419');
INSERT INTO `kota` VALUES (362, 27, 'Kabupaten', 'Polewali Mandar', '91311');
INSERT INTO `kota` VALUES (363, 11, 'Kabupaten', 'Ponorogo', '63411');
INSERT INTO `kota` VALUES (364, 12, 'Kabupaten', 'Pontianak', '78971');
INSERT INTO `kota` VALUES (365, 12, 'Kota', 'Pontianak', '78112');
INSERT INTO `kota` VALUES (366, 29, 'Kabupaten', 'Poso', '94615');
INSERT INTO `kota` VALUES (367, 33, 'Kota', 'Prabumulih', '31121');
INSERT INTO `kota` VALUES (368, 18, 'Kabupaten', 'Pringsewu', '35719');
INSERT INTO `kota` VALUES (369, 11, 'Kabupaten', 'Probolinggo', '67282');
INSERT INTO `kota` VALUES (370, 11, 'Kota', 'Probolinggo', '67215');
INSERT INTO `kota` VALUES (371, 14, 'Kabupaten', 'Pulang Pisau', '74811');
INSERT INTO `kota` VALUES (372, 20, 'Kabupaten', 'Pulau Morotai', '97771');
INSERT INTO `kota` VALUES (373, 24, 'Kabupaten', 'Puncak', '98981');
INSERT INTO `kota` VALUES (374, 24, 'Kabupaten', 'Puncak Jaya', '98979');
INSERT INTO `kota` VALUES (375, 10, 'Kabupaten', 'Purbalingga', '53312');
INSERT INTO `kota` VALUES (376, 9, 'Kabupaten', 'Purwakarta', '41119');
INSERT INTO `kota` VALUES (377, 10, 'Kabupaten', 'Purworejo', '54111');
INSERT INTO `kota` VALUES (378, 25, 'Kabupaten', 'Raja Ampat', '98489');
INSERT INTO `kota` VALUES (379, 4, 'Kabupaten', 'Rejang Lebong', '39112');
INSERT INTO `kota` VALUES (380, 10, 'Kabupaten', 'Rembang', '59219');
INSERT INTO `kota` VALUES (381, 26, 'Kabupaten', 'Rokan Hilir', '28992');
INSERT INTO `kota` VALUES (382, 26, 'Kabupaten', 'Rokan Hulu', '28511');
INSERT INTO `kota` VALUES (383, 23, 'Kabupaten', 'Rote Ndao', '85982');
INSERT INTO `kota` VALUES (384, 21, 'Kota', 'Sabang', '23512');
INSERT INTO `kota` VALUES (385, 23, 'Kabupaten', 'Sabu Raijua', '85391');
INSERT INTO `kota` VALUES (386, 10, 'Kota', 'Salatiga', '50711');
INSERT INTO `kota` VALUES (387, 15, 'Kota', 'Samarinda', '75133');
INSERT INTO `kota` VALUES (388, 12, 'Kabupaten', 'Sambas', '79453');
INSERT INTO `kota` VALUES (389, 34, 'Kabupaten', 'Samosir', '22392');
INSERT INTO `kota` VALUES (390, 11, 'Kabupaten', 'Sampang', '69219');
INSERT INTO `kota` VALUES (391, 12, 'Kabupaten', 'Sanggau', '78557');
INSERT INTO `kota` VALUES (392, 24, 'Kabupaten', 'Sarmi', '99373');
INSERT INTO `kota` VALUES (393, 8, 'Kabupaten', 'Sarolangun', '37419');
INSERT INTO `kota` VALUES (394, 32, 'Kota', 'Sawah Lunto', '27416');
INSERT INTO `kota` VALUES (395, 12, 'Kabupaten', 'Sekadau', '79583');
INSERT INTO `kota` VALUES (396, 28, 'Kabupaten', 'Selayar (Kepulauan Selayar)', '92812');
INSERT INTO `kota` VALUES (397, 4, 'Kabupaten', 'Seluma', '38811');
INSERT INTO `kota` VALUES (398, 10, 'Kabupaten', 'Semarang', '50511');
INSERT INTO `kota` VALUES (399, 10, 'Kota', 'Semarang', '50135');
INSERT INTO `kota` VALUES (400, 19, 'Kabupaten', 'Seram Bagian Barat', '97561');
INSERT INTO `kota` VALUES (401, 19, 'Kabupaten', 'Seram Bagian Timur', '97581');
INSERT INTO `kota` VALUES (402, 3, 'Kabupaten', 'Serang', '42182');
INSERT INTO `kota` VALUES (403, 3, 'Kota', 'Serang', '42111');
INSERT INTO `kota` VALUES (404, 34, 'Kabupaten', 'Serdang Bedagai', '20915');
INSERT INTO `kota` VALUES (405, 14, 'Kabupaten', 'Seruyan', '74211');
INSERT INTO `kota` VALUES (406, 26, 'Kabupaten', 'Siak', '28623');
INSERT INTO `kota` VALUES (407, 34, 'Kota', 'Sibolga', '22522');
INSERT INTO `kota` VALUES (408, 28, 'Kabupaten', 'Sidenreng Rappang/Rapang', '91613');
INSERT INTO `kota` VALUES (409, 11, 'Kabupaten', 'Sidoarjo', '61219');
INSERT INTO `kota` VALUES (410, 29, 'Kabupaten', 'Sigi', '94364');
INSERT INTO `kota` VALUES (411, 32, 'Kabupaten', 'Sijunjung (Sawah Lunto Sijunjung)', '27511');
INSERT INTO `kota` VALUES (412, 23, 'Kabupaten', 'Sikka', '86121');
INSERT INTO `kota` VALUES (413, 34, 'Kabupaten', 'Simalungun', '21162');
INSERT INTO `kota` VALUES (414, 21, 'Kabupaten', 'Simeulue', '23891');
INSERT INTO `kota` VALUES (415, 12, 'Kota', 'Singkawang', '79117');
INSERT INTO `kota` VALUES (416, 28, 'Kabupaten', 'Sinjai', '92615');
INSERT INTO `kota` VALUES (417, 12, 'Kabupaten', 'Sintang', '78619');
INSERT INTO `kota` VALUES (418, 11, 'Kabupaten', 'Situbondo', '68316');
INSERT INTO `kota` VALUES (419, 5, 'Kabupaten', 'Sleman', '55513');
INSERT INTO `kota` VALUES (420, 32, 'Kabupaten', 'Solok', '27365');
INSERT INTO `kota` VALUES (421, 32, 'Kota', 'Solok', '27315');
INSERT INTO `kota` VALUES (422, 32, 'Kabupaten', 'Solok Selatan', '27779');
INSERT INTO `kota` VALUES (423, 28, 'Kabupaten', 'Soppeng', '90812');
INSERT INTO `kota` VALUES (424, 25, 'Kabupaten', 'Sorong', '98431');
INSERT INTO `kota` VALUES (425, 25, 'Kota', 'Sorong', '98411');
INSERT INTO `kota` VALUES (426, 25, 'Kabupaten', 'Sorong Selatan', '98454');
INSERT INTO `kota` VALUES (427, 10, 'Kabupaten', 'Sragen', '57211');
INSERT INTO `kota` VALUES (428, 9, 'Kabupaten', 'Subang', '41215');
INSERT INTO `kota` VALUES (429, 21, 'Kota', 'Subulussalam', '24882');
INSERT INTO `kota` VALUES (430, 9, 'Kabupaten', 'Sukabumi', '43311');
INSERT INTO `kota` VALUES (431, 9, 'Kota', 'Sukabumi', '43114');
INSERT INTO `kota` VALUES (432, 14, 'Kabupaten', 'Sukamara', '74712');
INSERT INTO `kota` VALUES (433, 10, 'Kabupaten', 'Sukoharjo', '57514');
INSERT INTO `kota` VALUES (434, 23, 'Kabupaten', 'Sumba Barat', '87219');
INSERT INTO `kota` VALUES (435, 23, 'Kabupaten', 'Sumba Barat Daya', '87453');
INSERT INTO `kota` VALUES (436, 23, 'Kabupaten', 'Sumba Tengah', '87358');
INSERT INTO `kota` VALUES (437, 23, 'Kabupaten', 'Sumba Timur', '87112');
INSERT INTO `kota` VALUES (438, 22, 'Kabupaten', 'Sumbawa', '84315');
INSERT INTO `kota` VALUES (439, 22, 'Kabupaten', 'Sumbawa Barat', '84419');
INSERT INTO `kota` VALUES (440, 9, 'Kabupaten', 'Sumedang', '45326');
INSERT INTO `kota` VALUES (441, 11, 'Kabupaten', 'Sumenep', '69413');
INSERT INTO `kota` VALUES (442, 8, 'Kota', 'Sungaipenuh', '37113');
INSERT INTO `kota` VALUES (443, 24, 'Kabupaten', 'Supiori', '98164');
INSERT INTO `kota` VALUES (444, 11, 'Kota', 'Surabaya', '60119');
INSERT INTO `kota` VALUES (445, 10, 'Kota', 'Surakarta (Solo)', '57113');
INSERT INTO `kota` VALUES (446, 13, 'Kabupaten', 'Tabalong', '71513');
INSERT INTO `kota` VALUES (447, 1, 'Kabupaten', 'Tabanan', '82119');
INSERT INTO `kota` VALUES (448, 28, 'Kabupaten', 'Takalar', '92212');
INSERT INTO `kota` VALUES (449, 25, 'Kabupaten', 'Tambrauw', '98475');
INSERT INTO `kota` VALUES (450, 16, 'Kabupaten', 'Tana Tidung', '77611');
INSERT INTO `kota` VALUES (451, 28, 'Kabupaten', 'Tana Toraja', '91819');
INSERT INTO `kota` VALUES (452, 13, 'Kabupaten', 'Tanah Bumbu', '72211');
INSERT INTO `kota` VALUES (453, 32, 'Kabupaten', 'Tanah Datar', '27211');
INSERT INTO `kota` VALUES (454, 13, 'Kabupaten', 'Tanah Laut', '70811');
INSERT INTO `kota` VALUES (455, 3, 'Kabupaten', 'Tangerang', '15914');
INSERT INTO `kota` VALUES (456, 3, 'Kota', 'Tangerang', '15111');
INSERT INTO `kota` VALUES (457, 3, 'Kota', 'Tangerang Selatan', '15435');
INSERT INTO `kota` VALUES (458, 18, 'Kabupaten', 'Tanggamus', '35619');
INSERT INTO `kota` VALUES (459, 34, 'Kota', 'Tanjung Balai', '21321');
INSERT INTO `kota` VALUES (460, 8, 'Kabupaten', 'Tanjung Jabung Barat', '36513');
INSERT INTO `kota` VALUES (461, 8, 'Kabupaten', 'Tanjung Jabung Timur', '36719');
INSERT INTO `kota` VALUES (462, 17, 'Kota', 'Tanjung Pinang', '29111');
INSERT INTO `kota` VALUES (463, 34, 'Kabupaten', 'Tapanuli Selatan', '22742');
INSERT INTO `kota` VALUES (464, 34, 'Kabupaten', 'Tapanuli Tengah', '22611');
INSERT INTO `kota` VALUES (465, 34, 'Kabupaten', 'Tapanuli Utara', '22414');
INSERT INTO `kota` VALUES (466, 13, 'Kabupaten', 'Tapin', '71119');
INSERT INTO `kota` VALUES (467, 16, 'Kota', 'Tarakan', '77114');
INSERT INTO `kota` VALUES (468, 9, 'Kabupaten', 'Tasikmalaya', '46411');
INSERT INTO `kota` VALUES (469, 9, 'Kota', 'Tasikmalaya', '46116');
INSERT INTO `kota` VALUES (470, 34, 'Kota', 'Tebing Tinggi', '20632');
INSERT INTO `kota` VALUES (471, 8, 'Kabupaten', 'Tebo', '37519');
INSERT INTO `kota` VALUES (472, 10, 'Kabupaten', 'Tegal', '52419');
INSERT INTO `kota` VALUES (473, 10, 'Kota', 'Tegal', '52114');
INSERT INTO `kota` VALUES (474, 25, 'Kabupaten', 'Teluk Bintuni', '98551');
INSERT INTO `kota` VALUES (475, 25, 'Kabupaten', 'Teluk Wondama', '98591');
INSERT INTO `kota` VALUES (476, 10, 'Kabupaten', 'Temanggung', '56212');
INSERT INTO `kota` VALUES (477, 20, 'Kota', 'Ternate', '97714');
INSERT INTO `kota` VALUES (478, 20, 'Kota', 'Tidore Kepulauan', '97815');
INSERT INTO `kota` VALUES (479, 23, 'Kabupaten', 'Timor Tengah Selatan', '85562');
INSERT INTO `kota` VALUES (480, 23, 'Kabupaten', 'Timor Tengah Utara', '85612');
INSERT INTO `kota` VALUES (481, 34, 'Kabupaten', 'Toba Samosir', '22316');
INSERT INTO `kota` VALUES (482, 29, 'Kabupaten', 'Tojo Una-Una', '94683');
INSERT INTO `kota` VALUES (483, 29, 'Kabupaten', 'Toli-Toli', '94542');
INSERT INTO `kota` VALUES (484, 24, 'Kabupaten', 'Tolikara', '99411');
INSERT INTO `kota` VALUES (485, 31, 'Kota', 'Tomohon', '95416');
INSERT INTO `kota` VALUES (486, 28, 'Kabupaten', 'Toraja Utara', '91831');
INSERT INTO `kota` VALUES (487, 11, 'Kabupaten', 'Trenggalek', '66312');
INSERT INTO `kota` VALUES (488, 19, 'Kota', 'Tual', '97612');
INSERT INTO `kota` VALUES (489, 11, 'Kabupaten', 'Tuban', '62319');
INSERT INTO `kota` VALUES (490, 18, 'Kabupaten', 'Tulang Bawang', '34613');
INSERT INTO `kota` VALUES (491, 18, 'Kabupaten', 'Tulang Bawang Barat', '34419');
INSERT INTO `kota` VALUES (492, 11, 'Kabupaten', 'Tulungagung', '66212');
INSERT INTO `kota` VALUES (493, 28, 'Kabupaten', 'Wajo', '90911');
INSERT INTO `kota` VALUES (494, 30, 'Kabupaten', 'Wakatobi', '93791');
INSERT INTO `kota` VALUES (495, 24, 'Kabupaten', 'Waropen', '98269');
INSERT INTO `kota` VALUES (496, 18, 'Kabupaten', 'Way Kanan', '34711');
INSERT INTO `kota` VALUES (497, 10, 'Kabupaten', 'Wonogiri', '57619');
INSERT INTO `kota` VALUES (498, 10, 'Kabupaten', 'Wonosobo', '56311');
INSERT INTO `kota` VALUES (499, 24, 'Kabupaten', 'Yahukimo', '99041');
INSERT INTO `kota` VALUES (500, 24, 'Kabupaten', 'Yalimo', '99481');
INSERT INTO `kota` VALUES (501, 5, 'Kota', 'Yogyakarta', '55111');
COMMIT;

-- ----------------------------
-- Table structure for pengunjung
-- ----------------------------
DROP TABLE IF EXISTS `pengunjung`;
CREATE TABLE `pengunjung` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produk` int(11) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `dikunjungi` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`id_produk`),
  KEY `pengunjung_produk` (`id_produk`),
  CONSTRAINT `pengunjung_produk` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pengunjung
-- ----------------------------
BEGIN;
INSERT INTO `pengunjung` VALUES (1, 2, '::1', '2021-09-02 02:44:43');
INSERT INTO `pengunjung` VALUES (3, 2, '::1', '2021-09-02 02:45:09');
INSERT INTO `pengunjung` VALUES (4, 14, '::1', '2021-09-02 02:46:49');
INSERT INTO `pengunjung` VALUES (5, 14, '::1', '2021-09-02 02:47:00');
INSERT INTO `pengunjung` VALUES (6, 14, '::1', '2021-09-02 03:09:04');
INSERT INTO `pengunjung` VALUES (7, 2, '::1', '2021-09-13 02:53:24');
INSERT INTO `pengunjung` VALUES (8, 1, '::1', '2021-09-13 02:53:31');
INSERT INTO `pengunjung` VALUES (9, 2, '::1', '2021-09-13 02:53:37');
INSERT INTO `pengunjung` VALUES (10, 1, '::1', '2021-09-13 02:53:44');
INSERT INTO `pengunjung` VALUES (11, 2, '::1', '2021-09-16 07:59:32');
INSERT INTO `pengunjung` VALUES (12, 2, '::1', '2021-09-16 07:59:51');
INSERT INTO `pengunjung` VALUES (13, 14, '::1', '2021-09-17 09:09:48');
INSERT INTO `pengunjung` VALUES (14, 1, '::1', '2021-09-17 09:09:52');
INSERT INTO `pengunjung` VALUES (15, 1, '::1', '2021-09-17 09:13:36');
INSERT INTO `pengunjung` VALUES (16, 1, '182.253.176.202', '2021-09-17 18:26:19');
INSERT INTO `pengunjung` VALUES (17, 1, '182.253.176.202', '2021-09-17 18:27:48');
INSERT INTO `pengunjung` VALUES (18, 1, '182.253.178.220', '2021-09-20 13:56:06');
INSERT INTO `pengunjung` VALUES (19, 14, '182.253.178.220', '2021-09-20 14:18:01');
INSERT INTO `pengunjung` VALUES (20, 2, '160.19.66.208', '2021-09-20 14:45:35');
INSERT INTO `pengunjung` VALUES (21, 2, '160.19.66.208', '2021-09-20 14:48:53');
INSERT INTO `pengunjung` VALUES (22, 1, '160.19.66.208', '2021-09-20 14:49:06');
INSERT INTO `pengunjung` VALUES (23, 1, '182.253.178.220', '2021-09-20 14:53:18');
INSERT INTO `pengunjung` VALUES (24, 2, '182.253.178.220', '2021-09-20 14:58:31');
INSERT INTO `pengunjung` VALUES (25, 2, '182.253.178.220', '2021-09-20 15:33:38');
INSERT INTO `pengunjung` VALUES (26, 16, '182.253.178.220', '2021-09-20 15:51:37');
INSERT INTO `pengunjung` VALUES (27, 1, '160.19.66.208', '2021-09-20 15:52:07');
INSERT INTO `pengunjung` VALUES (28, 2, '182.253.178.220', '2021-09-20 15:52:35');
INSERT INTO `pengunjung` VALUES (29, 17, '202.80.217.28', '2021-09-21 22:06:50');
INSERT INTO `pengunjung` VALUES (30, 2, '114.142.173.23', '2021-09-22 09:42:59');
INSERT INTO `pengunjung` VALUES (31, 19, '180.251.133.236', '2021-09-22 12:51:22');
INSERT INTO `pengunjung` VALUES (32, 19, '180.251.133.236', '2021-09-22 12:52:08');
INSERT INTO `pengunjung` VALUES (33, 19, '180.251.133.236', '2021-09-22 12:52:21');
INSERT INTO `pengunjung` VALUES (34, 19, '180.251.133.236', '2021-09-22 13:05:18');
INSERT INTO `pengunjung` VALUES (35, 19, '180.251.133.236', '2021-09-22 13:07:41');
INSERT INTO `pengunjung` VALUES (36, 2, '180.251.133.236', '2021-09-22 13:38:06');
INSERT INTO `pengunjung` VALUES (37, 1, '103.148.113.86', '2021-09-23 08:39:48');
INSERT INTO `pengunjung` VALUES (38, 21, '103.148.113.86', '2021-09-23 10:35:28');
INSERT INTO `pengunjung` VALUES (39, 21, '103.148.113.86', '2021-09-23 10:35:55');
INSERT INTO `pengunjung` VALUES (40, 21, '103.148.113.86', '2021-09-23 10:36:37');
INSERT INTO `pengunjung` VALUES (41, 21, '103.148.113.86', '2021-09-23 10:37:27');
INSERT INTO `pengunjung` VALUES (42, 23, '103.148.113.86', '2021-09-23 10:51:59');
INSERT INTO `pengunjung` VALUES (43, 23, '103.148.113.86', '2021-09-23 10:52:21');
INSERT INTO `pengunjung` VALUES (44, 1, '103.148.113.86', '2021-09-23 13:21:51');
INSERT INTO `pengunjung` VALUES (45, 25, '103.148.113.86', '2021-09-23 13:54:22');
INSERT INTO `pengunjung` VALUES (46, 1, '103.148.113.86', '2021-09-23 13:54:27');
INSERT INTO `pengunjung` VALUES (47, 2, '103.148.113.86', '2021-09-23 13:54:33');
INSERT INTO `pengunjung` VALUES (48, 28, '103.148.113.86', '2021-10-06 07:18:43');
INSERT INTO `pengunjung` VALUES (49, 26, '140.213.25.236', '2021-10-06 09:45:34');
INSERT INTO `pengunjung` VALUES (50, 23, '222.124.127.57', '2021-10-06 09:52:33');
INSERT INTO `pengunjung` VALUES (51, 26, '140.213.25.236', '2021-10-06 10:13:26');
INSERT INTO `pengunjung` VALUES (52, 25, '36.71.238.55', '2021-10-09 14:02:53');
INSERT INTO `pengunjung` VALUES (53, 25, '140.213.2.104', '2021-10-10 08:14:49');
INSERT INTO `pengunjung` VALUES (54, 1, '127.0.0.1', '2021-10-21 03:49:49');
COMMIT;

-- ----------------------------
-- Table structure for produk
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_toko` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `gambar` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `nama` varchar(255) DEFAULT NULL,
  `stok` decimal(12,0) DEFAULT '0',
  `rating` decimal(12,0) DEFAULT '0',
  `terjual` decimal(12,0) DEFAULT '0',
  `dilihat` decimal(12,0) DEFAULT '0',
  `deskripsi` text,
  `varian` text,
  `harga` decimal(12,0) DEFAULT NULL,
  `harga_varian` text,
  `berat_varian` text,
  `dibuat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produk_kategori` (`id_kategori`),
  KEY `produk_toko` (`id_toko`),
  CONSTRAINT `produk_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `produk_toko` FOREIGN KEY (`id_toko`) REFERENCES `toko` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of produk
-- ----------------------------
BEGIN;
INSERT INTO `produk` VALUES (1, 1, 1, '[\"\\/uploads\\/produk\\/apel-malang-kalah-bersaing-dengan-apel-impor-_140223154455-269.jpg\"]', 'Apel Malang', 3, 0, 0, 14, 'Deskripsi Produk', '[\"Manis\"]', 15000, '[\"15000\"]', '[\"1\"]', '2021-08-12 07:27:15');
INSERT INTO `produk` VALUES (2, 1, 2, '[\"\\/uploads\\/produk\\/mengenal-6-manfaat-buah-strawberry-yang-baik-untuk-kesehatan-1572948452.jpg\"]', 'Strawberry Manis', 3, 0, 0, 16, 'Deskripsi Produk 2', '[\"Manis\"]', 35000, '[\"35000\"]', '[\"1\"]', '2021-08-12 07:27:20');
INSERT INTO `produk` VALUES (14, 1, 6, '[\"\\/uploads\\/produk\\/download_(1).jfif\"]', 'Mangga Arum Manis', 10, 0, 0, 0, 'deskripsi', '[\"Manis\"]', 25000, '[\"25000\"]', '[\"1\"]', NULL);
INSERT INTO `produk` VALUES (15, 1, 9, '[\"\\/uploads\\/produk\\/download_(2).jfif\"]', 'Pisang Ambon', 12, 0, 0, 0, 'Pisang manis', '[\"Manis\"]', 15000, '[\"15000\"]', '[\"1\"]', NULL);
INSERT INTO `produk` VALUES (16, 1, 8, '[\"\\/uploads\\/produk\\/download_(3).jfif\"]', 'Nanas Super', 5, 0, 0, 2, 'Manis', '[\"Manis\"]', 16000, '[\"16000\"]', '[\"1\"]', NULL);
INSERT INTO `produk` VALUES (17, 1, 7, '[\"\\/uploads\\/produk\\/images.jfif\"]', 'Jambu Air', 14, 0, 0, 2, 'Manis', '[\"Manis\"]', 13000, '[\"13000\"]', '[\"1\"]', NULL);
INSERT INTO `produk` VALUES (19, 7, 1, '[\"\\/uploads\\/produk\\/3.jpg\"]', 'Apel Fuji', 10, 0, 0, 4, 'enakkkk guyss', '[\"jssgdwhd\"]', 5000, '[\"5000\"]', '[\"5\"]', NULL);
INSERT INTO `produk` VALUES (20, 8, 7, '[\"\\/uploads\\/produk\\/syal_shibori.jpg\"]', 'Syal Shibori', 100, 0, 0, 0, 'Syal (Persia شال, Shāl, dari bahasa Sanskerta: साडी śāṭī)[1] adalah kain pakaian sederhana, dipakai secara longgar di atas bahu, tubuh bagian atas dan lengan, kadang-kadang juga di atas kepala. Biasanya berupa sehelai kain persegi panjang, sering dilipat untuk membuat segitiga tetapi juga bisa memang berbentuk segitiga dari awal.', '[\"Syal Shibori\"]', 100000, '[\"100000\"]', '[\"10\"]', NULL);
INSERT INTO `produk` VALUES (21, 8, 7, '[\"\\/uploads\\/produk\\/kain_shibori_1.jpeg\"]', 'Kain Shibori 1', 100, 0, 0, 0, 'Syal (Persia شال, Shāl, dari bahasa Sanskerta: साडी śāṭī)[1] adalah kain pakaian sederhana, dipakai secara longgar di atas bahu, tubuh bagian atas dan lengan, kadang-kadang juga di atas kepala. Biasanya berupa sehelai kain persegi panjang, sering dilipat untuk membuat segitiga tetapi juga bisa memang berbentuk segitiga dari awal.', '[\"Kain Shibori 2\"]', 100000, '[\"100000\"]', '[\"10\"]', NULL);
INSERT INTO `produk` VALUES (22, 8, 7, '[\"\\/uploads\\/produk\\/kain_shibori_2.jpeg\"]', 'Kain Shibori 2', 100, 0, 0, 0, 'Syal (Persia شال, Shāl, dari bahasa Sanskerta: साडी śāṭī)[1] adalah kain pakaian sederhana, dipakai secara longgar di atas bahu, tubuh bagian atas dan lengan, kadang-kadang juga di atas kepala. Biasanya berupa sehelai kain persegi panjang, sering dilipat untuk membuat segitiga tetapi juga bisa memang berbentuk segitiga dari awal.', '[\"Kain Shibori 2\"]', 100000, '[\"100000\"]', '[\"10\"]', NULL);
INSERT INTO `produk` VALUES (23, 8, 1, '[\"\\/uploads\\/produk\\/kastangel.jpeg\"]', 'Kue', 100, 0, 0, 2, '', '[\"Kastangel\"]', 100000, '[\"100000\"]', '[\"1000\"]', NULL);
INSERT INTO `produk` VALUES (24, 8, 6, '[\"\\/uploads\\/produk\\/kaos_shibori.jpg\"]', 'Kaos', 100, 0, 0, 0, '', '[\"Kaos Shibori\"]', 100000, '[\"100000\"]', '[\"1000\"]', NULL);
INSERT INTO `produk` VALUES (25, 8, 1, '[\"\\/uploads\\/produk\\/nastar.jpeg\"]', 'Kue Nastar', 100, 0, 0, 6, '', '[\"Nastar\"]', 90000, '[\"90000\"]', '[\"100\"]', NULL);
INSERT INTO `produk` VALUES (26, 8, 1, '[\"\\/uploads\\/produk\\/choco_chip.jpeg\"]', 'Kue Choco Chip', 100, 0, 0, 4, '', '[\"Choco Chip\"]', 85000, '[\"85000\"]', '[\"1000\"]', NULL);
INSERT INTO `produk` VALUES (27, 8, 1, '[\"\\/uploads\\/produk\\/nastar1.jpeg\"]', 'Kue Nastar', 100, 0, 0, 0, '', '[\"Kue Nastar\"]', 85000, '[\"85000\"]', '[\"1000\"]', NULL);
INSERT INTO `produk` VALUES (28, 8, 8, '[\"\\/uploads\\/produk\\/masker_kain_shibori_warna.jpg\"]', 'Masker Kain', 100, 0, 0, 2, '', '[\"Shibori 1\"]', 12500, '[\"12500\"]', '[\"100\"]', NULL);
COMMIT;

-- ----------------------------
-- Table structure for produk_rekomendasi
-- ----------------------------
DROP TABLE IF EXISTS `produk_rekomendasi`;
CREATE TABLE `produk_rekomendasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produk` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produkrekomendasi_produk` (`id_produk`),
  CONSTRAINT `produkrekomendasi_produk` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of produk_rekomendasi
-- ----------------------------
BEGIN;
INSERT INTO `produk_rekomendasi` VALUES (2, 1);
INSERT INTO `produk_rekomendasi` VALUES (7, 21);
INSERT INTO `produk_rekomendasi` VALUES (9, 24);
INSERT INTO `produk_rekomendasi` VALUES (8, 25);
INSERT INTO `produk_rekomendasi` VALUES (11, 26);
INSERT INTO `produk_rekomendasi` VALUES (10, 28);
COMMIT;

-- ----------------------------
-- Table structure for produk_ulasan
-- ----------------------------
DROP TABLE IF EXISTS `produk_ulasan`;
CREATE TABLE `produk_ulasan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_akun` int(11) DEFAULT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `nilai` int(1) DEFAULT NULL,
  `ulasan` varchar(255) DEFAULT NULL,
  `dibuat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `produkulasan_produk` (`id_produk`),
  KEY `produkulasan_akun` (`id_akun`),
  CONSTRAINT `produkulasan_akun` FOREIGN KEY (`id_akun`) REFERENCES `akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `produkulasan_produk` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of produk_ulasan
-- ----------------------------
BEGIN;
INSERT INTO `produk_ulasan` VALUES (6, 2, 1, 5, '123', '2021-08-23 00:17:34');
COMMIT;

-- ----------------------------
-- Table structure for provinsi
-- ----------------------------
DROP TABLE IF EXISTS `provinsi`;
CREATE TABLE `provinsi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of provinsi
-- ----------------------------
BEGIN;
INSERT INTO `provinsi` VALUES (1, 'Bali');
INSERT INTO `provinsi` VALUES (2, 'Bangka Belitung');
INSERT INTO `provinsi` VALUES (3, 'Banten');
INSERT INTO `provinsi` VALUES (4, 'Bengkulu');
INSERT INTO `provinsi` VALUES (5, 'DI Yogyakarta');
INSERT INTO `provinsi` VALUES (6, 'DKI Jakarta');
INSERT INTO `provinsi` VALUES (7, 'Gorontalo');
INSERT INTO `provinsi` VALUES (8, 'Jambi');
INSERT INTO `provinsi` VALUES (9, 'Jawa Barat');
INSERT INTO `provinsi` VALUES (10, 'Jawa Tengah');
INSERT INTO `provinsi` VALUES (11, 'Jawa Timur');
INSERT INTO `provinsi` VALUES (12, 'Kalimantan Barat');
INSERT INTO `provinsi` VALUES (13, 'Kalimantan Selatan');
INSERT INTO `provinsi` VALUES (14, 'Kalimantan Tengah');
INSERT INTO `provinsi` VALUES (15, 'Kalimantan Timur');
INSERT INTO `provinsi` VALUES (16, 'Kalimantan Utara');
INSERT INTO `provinsi` VALUES (17, 'Kepulauan Riau');
INSERT INTO `provinsi` VALUES (18, 'Lampung');
INSERT INTO `provinsi` VALUES (19, 'Maluku');
INSERT INTO `provinsi` VALUES (20, 'Maluku Utara');
INSERT INTO `provinsi` VALUES (21, 'Nanggroe Aceh Darussalam (NAD)');
INSERT INTO `provinsi` VALUES (22, 'Nusa Tenggara Barat (NTB)');
INSERT INTO `provinsi` VALUES (23, 'Nusa Tenggara Timur (NTT)');
INSERT INTO `provinsi` VALUES (24, 'Papua');
INSERT INTO `provinsi` VALUES (25, 'Papua Barat');
INSERT INTO `provinsi` VALUES (26, 'Riau');
INSERT INTO `provinsi` VALUES (27, 'Sulawesi Barat');
INSERT INTO `provinsi` VALUES (28, 'Sulawesi Selatan');
INSERT INTO `provinsi` VALUES (29, 'Sulawesi Tengah');
INSERT INTO `provinsi` VALUES (30, 'Sulawesi Tenggara');
INSERT INTO `provinsi` VALUES (31, 'Sulawesi Utara');
INSERT INTO `provinsi` VALUES (32, 'Sumatera Barat');
INSERT INTO `provinsi` VALUES (33, 'Sumatera Selatan');
INSERT INTO `provinsi` VALUES (34, 'Sumatera Utara');
COMMIT;

-- ----------------------------
-- Table structure for toko
-- ----------------------------
DROP TABLE IF EXISTS `toko`;
CREATE TABLE `toko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_akun` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `deskripsi_singkat` varchar(255) DEFAULT NULL,
  `telepon` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `provinsi` varchar(255) DEFAULT NULL,
  `kota` varchar(255) DEFAULT NULL,
  `id_provinsi` int(11) DEFAULT NULL,
  `id_kota` int(11) DEFAULT NULL,
  `disetujui` int(1) DEFAULT '0',
  `dibuat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `toko_akun` (`id_akun`),
  CONSTRAINT `toko_akun` FOREIGN KEY (`id_akun`) REFERENCES `akun` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of toko
-- ----------------------------
BEGIN;
INSERT INTO `toko` VALUES (1, 2, 'Toko Randi 123', 'Chia Seed - Madu Al Shifa - Habbatusauda Cap Kurma Ajwa - Paket JSR', '082126833236', 'cimahi', 'randiekas_', 'Jawa Barat', 'Tasikmalaya', 9, 468, 1, '2021-09-02 10:33:17');
INSERT INTO `toko` VALUES (5, 19, 'randi eka', 'deskripsi', '082126833236', 'tess', '@randieks', 'Bangka Belitung', 'Bangka Tengah', 2, 30, 1, '2021-09-02 10:33:21');
INSERT INTO `toko` VALUES (6, 20, 'Toko Siswa', 'Penjualan Baju Siswa', '0829292929', NULL, 'siswa', 'DI Yogyakarta', 'Yogyakarta', 5, 501, 1, NULL);
INSERT INTO `toko` VALUES (7, 22, 'Dhea', 'Oh mangtap sangat', '089786764546697765454665755656565645', 'Kp cikuya lebak rt 04 rw 13', '@dheaamelia413', 'Jawa Barat', 'Bandung', 9, 24, 0, NULL);
INSERT INTO `toko` VALUES (8, 24, 'H&R Store', 'Toko penjual obat herbal produk HNI', '081220083232', 'Jl. Pagarsih Gg. Citepus Dalam II No. 23 RT.007/RW.003 Kel. Cibadak, Kec. Astana Anyar Kota Bandung 40241', NULL, 'Jawa Barat', 'Bandung', 9, 22, 1, NULL);
COMMIT;

-- ----------------------------
-- Table structure for transaksi
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_toko` int(11) DEFAULT NULL,
  `id_akun` int(11) DEFAULT NULL,
  `id_provinsi` int(11) DEFAULT NULL,
  `id_kota` int(11) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `layanan_pengiriman` varchar(255) DEFAULT NULL,
  `telepon_penerima` varchar(20) DEFAULT NULL,
  `bank_transfer` varchar(50) DEFAULT NULL,
  `total_berat` decimal(12,0) DEFAULT NULL,
  `total_biaya_pengiriman` decimal(12,0) DEFAULT NULL,
  `total_harga` decimal(12,0) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `bukti_pembayaran` varchar(255) DEFAULT NULL,
  `dibuat` datetime DEFAULT NULL,
  `diubah` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaksi_akun` (`id_akun`),
  KEY `transaksi_provinsi` (`id_provinsi`),
  KEY `transaksi_kota` (`id_kota`),
  CONSTRAINT `transaksi_akun` FOREIGN KEY (`id_akun`) REFERENCES `akun` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `transaksi_kota` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `transaksi_provinsi` FOREIGN KEY (`id_provinsi`) REFERENCES `provinsi` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transaksi
-- ----------------------------
BEGIN;
INSERT INTO `transaksi` VALUES (20, 1, 2, 1, 32, 'ciuyah', 'Jalur Nugraha Ekakurir (JNE) - OKE - 52.000', '082126833236', 'BNI - 1202144193', 2000, 52000, 20000, 'menunggu_pembayaran', '/uploads/buktipembayaran/aPCtFOapCwiEewn5lJD44ahdAkqx0GlRO9CA45wY6.png', '2021-08-22 08:13:21', NULL);
INSERT INTO `transaksi` VALUES (26, 1, 2, 9, 23, 'bandung', 'Jalur Nugraha Ekakurir (JNE) - REG - 12.000', '72334782378', 'BNI - 1202144193', 1, 12000, 16000, 'menunggu_pembayaran', NULL, '2021-09-20 15:51:59', NULL);
INSERT INTO `transaksi` VALUES (27, 1, 20, 9, 23, 'Bandung', 'Jalur Nugraha Ekakurir (JNE) - REG - 12.000', '128319230', 'BNI - 1202144193', 1, 12000, 35000, 'menunggu_konfirmasi_penjual', '/uploads/buktipembayaran/detergent.png', '2021-09-20 15:53:00', NULL);
INSERT INTO `transaksi` VALUES (28, 1, 22, 5, 135, 'dhedadvcbvhgjhkhjkj', 'Jalur Nugraha Ekakurir (JNE) - OKE - 16.000', '76756898765432456789', 'BNI - 1202144193', 0, 16000, 0, 'menunggu_konfirmasi_penjual', '/uploads/buktipembayaran/2.jpg', '2021-09-22 09:45:08', NULL);
INSERT INTO `transaksi` VALUES (29, 7, 23, 3, 403, 'jhdjehefgehf', 'Jalur Nugraha Ekakurir (JNE) - OKE - 12.000', '667890987654', 'BCA - 1202144193', 10, 12000, 10000, 'menunggu_konfirmasi_penjual', '/uploads/buktipembayaran/5.jpg', '2021-09-22 13:10:47', NULL);
INSERT INTO `transaksi` VALUES (30, 1, 23, 2, 30, '', 'Jalur Nugraha Ekakurir (JNE) - OKE - 38.000', '67645678945', '0', 1, 38000, 35000, 'menunggu_pembayaran', NULL, '2021-09-22 13:38:51', NULL);
INSERT INTO `transaksi` VALUES (31, 8, 24, 9, 107, 'Jl. Kerkof Kihapit Barat Gg. Warga No. 40 RT. 02/RW.09 Kel. Leuwigajah, Kec. Cimahi Selatan Kota Cimahi 40532', 'Jalur Nugraha Ekakurir (JNE) - CTCYES - 10.000', '081220083232', 'BCA - 1202144193', 100, 10000, 12500, 'sudah_dikirim', '/uploads/buktipembayaran/downloadMandiri15325037976451.jpg', '2021-10-06 07:20:35', NULL);
COMMIT;

-- ----------------------------
-- Table structure for transaksi_detil
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_detil`;
CREATE TABLE `transaksi_detil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` int(11) DEFAULT NULL,
  `id_produk` int(11) DEFAULT NULL,
  `varian` varchar(255) DEFAULT NULL,
  `jumlah` varchar(255) DEFAULT NULL,
  `harga` decimal(12,0) DEFAULT NULL,
  `berat` decimal(12,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaksidetil_produk` (`id_produk`),
  KEY `transaksidetil_transaksi` (`id_transaksi`),
  CONSTRAINT `transaksidetil_produk` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `transaksidetil_transaksi` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transaksi_detil
-- ----------------------------
BEGIN;
INSERT INTO `transaksi_detil` VALUES (37, 20, 1, '32 Inch', '2', 10000, 1000);
INSERT INTO `transaksi_detil` VALUES (43, 26, 16, 'Manis', '1', 16000, 1);
INSERT INTO `transaksi_detil` VALUES (44, 27, 2, 'Manis', '1', 35000, 1);
INSERT INTO `transaksi_detil` VALUES (45, 28, 2, 'Manis', '0', 35000, 1);
INSERT INTO `transaksi_detil` VALUES (46, 29, 19, 'jssgdwhd', '1', 5000, 5);
INSERT INTO `transaksi_detil` VALUES (47, 29, 19, 'jssgdwhd', '1', 5000, 5);
INSERT INTO `transaksi_detil` VALUES (48, 30, 2, 'Manis', '1', 35000, 1);
INSERT INTO `transaksi_detil` VALUES (49, 31, 28, 'Shibori 1', '1', 12500, 100);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
