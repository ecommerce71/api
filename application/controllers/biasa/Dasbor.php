<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasbor extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}
	
	public function index(){

		$response["status"]		= true;
		$response["message"]	= "";
		$tahun					= date("Y");
		$id_toko				= $this->db->query("select id from toko where id_akun = ".$this->jwt->id_akun." limit 0,1")->last_row()->id;
		for($x=1; $x<=12;$x++){

			$kunjungan[]	= (int) $this->db->query("
				select 
					count(pengunjung.id) as total 
				from 
					pengunjung 
				left join
					produk
					on
						pengunjung.id_produk = produk.id
				where 
					produk.id_toko = {$id_toko}
				and
					year(pengunjung.dikunjungi) = '{$tahun}' 
				and 
					month(pengunjung.dikunjungi) = '{$x}'")
				->last_row()
				->total;
			
			$pengunjung[]	= (int) $this->db->query("
				select 
					count(DISTINCT(ip)) as total
				from 
					pengunjung 
				left join
					produk
					on
						pengunjung.id_produk = produk.id
				where 
					produk.id_toko = {$id_toko}
				and
					year(pengunjung.dikunjungi) = '{$tahun}' 
				and 
					month(pengunjung.dikunjungi) = '{$x}'")
				->last_row()
				->total;

			$penjualan[]	= (int) $this->db->query("
				select 
					count(transaksi.id) as total
				from 
					transaksi
				left join 
					transaksi_detil
					on
						transaksi_detil.id_transaksi = transaksi.id
				left join
					produk
					on
						produk.id = transaksi_detil.id_produk
				where 
					produk.id_toko = {$id_toko}
				and
					year(transaksi.dibuat) = '{$tahun}' 
				and 
					month(transaksi.dibuat) = '{$x}'")
				->last_row()
				->total;
		}

		$produkSeringDilihat	= $this->db->query("
				select 
					produk.nama,
					pengunjung.id_produk, 
					count(pengunjung.id) as total 
				from 
					pengunjung 
				left join
					produk
					on
						produk.id = pengunjung.id_produk
				where 
					year(pengunjung.dikunjungi) = '{$tahun}' 
				group by 
					pengunjung.id_produk
				order by 
					total desc
				limit 0,5")
				->result();
		
		$produkSeringDibeli		= $this->db->query("
				select 
					produk.nama,
					transaksi_detil.id_produk, 
					count(transaksi.id) as total 
				from 
					transaksi
				left join 
					transaksi_detil
					on
						transaksi_detil.id_transaksi = transaksi.id
				left join
					produk
					on
						produk.id = transaksi_detil.id_produk
				where 
					year(transaksi.dibuat) = '{$tahun}' 
				group by 
					transaksi_detil.id_produk
				order by 
					total desc
				limit 0,5")
				->result();

		$response["data"]["kunjungan"]				= $kunjungan;
		$response["data"]["pengunjung"]				= $pengunjung;
		$response["data"]["penjualan"]				= $penjualan;
		$response["data"]["produkSeringDilihat"]	= $produkSeringDilihat;
		$response["data"]["produkSeringDibeli"]		= $produkSeringDibeli;

		json($response);
	}
}
