<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}
	
	public function index(){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db->get("bank")->result();
		json($response);
	}

	public function hapus($id=0){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["status"]		= $this->db
				->query("
					delete
					from 
						bank
					where 
						id = ".$id."
				");
		json($response);
	}

	public function simpan(){
		$response["status"]		= true;
		$response["message"]	= "";

		$post 					= (object) post_data();
		
		$data["nama"]			= $post->nama;
		$data["rekening"]		= $post->rekening;
		
		if(@$post->id){
			$this->db->where("id", $post->id);
			$response["status"]		= $this->db->update("bank", $data);
		}else{
			$response["status"]		= $this->db->insert("bank", $data);
		}
		
		json($response);
	}

}
