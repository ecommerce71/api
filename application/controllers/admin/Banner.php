<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}
	
	public function index(){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db
				->query("
					select * 
					from banner 
				")
				->result();
		json($response);
	}

	public function hapus($id=0){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["status"]		= $this->db
				->query("
					delete
					from banner
					where 
						id = ".$id."
				");
		json($response);
	}

	public function tambah(){
		$response["status"]		= true;
		$response["message"]	= "";

		$post 					= (Object) post_data();
		
		$data["gambar"]			= $post->gambar;
		$data["link"]			= $post->link;
		
		if(@$post->id){
			$this->db->where("id", $post->id);
			$response["status"]		= $this->db->update("banner", $data);
		}else{
			$response["status"]		= $this->db->insert("banner", $data);
		}
		
		json($response);
	}

}
