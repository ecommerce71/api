<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->jwt				= cek_akses();
	}
	
	public function index(){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["data"]		= $this->db
				->query("
					select 
						*,
						(select count(id) from produk where id_kategori=kategori.id) as total_produk
					from kategori 
				")
				->result();
		json($response);
	}

	public function hapus($id=0){
		$response["status"]		= true;
		$response["message"]	= "";

		$response["status"]		= $this->db
				->query("
					delete
					from kategori
					where 
						id = ".$id."
				");
		json($response);
	}

	public function tambah(){
		$response["status"]		= true;
		$response["message"]	= "";

		$post 					= (Object) post_data();
		
		$data["gambar"]			= $post->gambar;
		$data["nama"]			= $post->nama;
		
		if(@$post->id){
			$this->db->where("id", $post->id);
			$response["status"]		= $this->db->update("kategori", $data);
		}else{
			$response["status"]		= $this->db->insert("kategori", $data);
		}
		
		json($response);
	}

}
